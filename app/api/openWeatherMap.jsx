var axios = require('axios');

const OPEN_WEATHER_MAP_URL = 'http://api.openweathermap.org/data/2.5/weather?appid=2f77729567768559c4ed1b281f4220e3&units=metric';

module.exports = {
	getTemp: getTemp
};

function getTemp(location) {
	var encodedLocation = encodeURIComponent(location);
	var requestUrl = `${OPEN_WEATHER_MAP_URL}&q=${encodedLocation}`;

	console.log(requestUrl)

	 return axios.get(requestUrl)
		.then(function(res) {
			if (res.data.cod && res.data.message) {
				throw new Error(res.data.message);
			}
			else {
				return res.data.main.temp;
			}
		}, function(error) {
			throw new Error(error.response.data.message);
		});
}