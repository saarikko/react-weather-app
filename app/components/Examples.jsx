var React = require('react');
var {Link} = require('react-router');

var Examples = (props) => {
	return (
		<div>
			<h1 className="text-center page-title">Examples</h1>
			<p>Here are location examples:</p>
			<ol>
				<li>
					<Link to='/?location=Tampere'>Tampere, Finland</Link>
				</li>
				<li>
					<Link to='/?location=Chengdu'>Chengdu, China</Link>
				</li>
			</ol>
		</div>
	);
}

module.exports = Examples;