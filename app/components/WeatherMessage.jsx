var React = require('react');


var WeatherMessage = ({temp, location}) => {
	return (
		<h3 className="text-center">{location}, temp: {temp}</h3>
	);	
}

module.exports = WeatherMessage;